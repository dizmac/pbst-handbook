# 👮‍♂️ Raid Response Leader Guide

The following is a guide produced by NeonSmashSwordALT (tell him of any issues in this guide, as well as any other questions). This guide outlines key information regarding the Raid Response Leader role. This should hopefully help new Raid Response Leaders and prevent them from making mistakes during their first few days of being a RRL. 


---

## Using raid-warnings 

When you are a RRL, you will have access to the #raid-warnings channel. That channel will be used when TMS raids the Pinewood Computer Core, or any other Pinewood facility. 

These are a few guidelines that you should follow when using the channel:
* Do not announce the raid **before the TMS raid host does**. There may be a chance where the raid might have been delayed, or cancelled altogether. Therefore, you're potentially mass pinging online users for a cancelled raid!
* Do not ping @everyone. We use @here to ping people for raid warnings. @here pings all online users, however, @Everyone pings every single person that has access to #raid-warnings. So, always use @here.
* Do not use the channel for anything else other than its purpose. The only purpose of this channel is to alert PBST of TMS raids. You are to **not** post things such as messages that can instigate drama between TMS and PBST, having a conversation on the actual channel etc.
* Use backup calls wisely. It is in your discretion to make a backup call when you feel like the time is right. However, you should have some common sense when requesting for backup. For example, **do not mass ping for backup within a short range of time.**

---

## In-game

If you're going to be attending as PBST and leading during a TMS raid, you are essentially responsible for every PBST participating in that server, especially if you're the only RRL+ in game. 

If there is a PBST member causing nuisance and not following any rules, here is what you should do:

* First, warn them of their wrongdoings and ask them to stop. Some of the time, this is all you need to do for them to stop and they would start listening to your orders. Depending on what they did though, you can still reward reduced, no points or even deduct points, such as if they used TMS tools while on PBST duty. 
* If they do not listen to you however and continue causing havoc, you can consider them a rogue PBST and put that person of PBST KoS. At that point, you can request a point deduction ranging from -5 all the way up to -15+. It is also best to give the trainers proof of their actions in order to validate your claims. 
* If that PBST member is causing too much of a havoc, such as him MRKing everyone or even exploiting, send a PIA call as soon as possible and wait for them to come. Make sure to tell a trainer about what happened. That member would be severly punished. 

If you catch any TMS members breaking the rules, and the raid host isn't aware of it, pm the raid host and tell them their name(s) and what they were doing. 

---

Being a RRL, you would also be tasked with effectively leading PBST and prevent TMS from achieving their objective(s). These are a few guides to leading successfully.

* Use effective tactics and strageties. Using strageties and tactics can really help while you're leading. These can be simple tactics/strageties such as using fake calls, spiltting into different groups, pushing as a team etc.
* Be a strong leader. You'll have to show PBST that you are a strong leader and have strong optimism. Be strong with your words and directly tell PBST where to go and what you will do to execute your orders. Actively update PBST on current circumstances, and ensure that everyone is trying their best out there. Motivate yourself and others, and victory is more likely.
* Tell everyone to use the Raid VC. These past few raids, it's now become a norm to use Raid VC everytime. We have improved dramatically during raids, and using Raid VC really helped. Even if you won't be talking, join the Raid VC and tell others to use it. If someone is annoying you or starts complaining in Raid VC, tell that person to stop. 
* Use KoS efficiently. One other perk of being a RRL is the ability of placing KoS orders during raids. This essentially allows you to order PBST to kill someone/a group on sight if they see them.  

**This SHOULD NOT be used lightly and shouldn't be abused**. When placing a KoS order on someone/group, make sure it meets these criterias:  
1. They are killing PBST,  
2. They are teaming with TMS,  
3. They are attempting to cause a meltdown, freezedown, cause chaos in the cargo bay etc.   

During the raid, you should also make sure you mark attendance for all the PBST that have attended the raid. This is usually done by doing attendance checks throughout the raid. Typically, doing 2-3 checks should be enough, 1 about 10 minutes into the raid, one in the middle of the raid, and one at the end of the raid. You should note down every PBST that responds to your attendance check.

The following is a generic format of an attendance check:

``[PBST] FIRST attendance check. If you are attending this raid as security, say "HERE".``  
``[PBST] SECOND attendance check. If you are attending this raid as security, say "HERE".``  
``[PBST] LAST attendance check. If you are attending this raid as security, say "HERE".``  
``[PBST] Attendance logged. If you felt like you wasn't logged, /w me so I can log you.``  

---

### Post Raid

If TMS did manage to suceed with their objectives, if it was either a meltdown or a freezedown, the TMS raid host would either initate a Yes Survivor Run or a No Survivor Run.

The following is a definition of both a YSR and a NSR, taken from the TMS handbook:

**Yes-Survivor Run:**
Help survivors escape in rockets.

**No-Survivor Run:**
Do not let anyone escape in rockets.

If TMS orders a Yes Survivor Run, the raid host would take PBST OFF KoS. You are to also take TMS OFF KoS. You are to make sure NO PBST member kill any TMS member, and if you catch one doing so, unless that TMS member initially started shooting the PBST member, alert them of their wrongdoing and reduce the amount of points he would get. If it was a worse case scenario, you could give him no points or even deduct points from him. 

After the NSR or YSR finishes, TMS would most likely take a screenshot together in any area of the game. It is highly suggested you do the same. At this point, TMS would take PBST off KoS, you should also take TMS off KoS.

When taking the screenshot, you should tell all PBST to meet up in a certain place and (typically) STS next to each other. As of the 23rd of October 2020, the PBST slogan is "We Are PBST!" Like TMS says "Glory To The Syndicate!", PBST should use the slogan when the meltdown timer reaches 0 and take a screenshot. 

:::danger DO NOT SAY "PBST ABOVE ALL"!!!
This saying is **really, really** frowned upon and you would receive harsh criticism if you told everyone to say "PBST Above All". This is because saying this is basically giving no credit to the other pro-core groups that helped PBST during the raid. Especially now that PBST has a new slogan, this really shouldn't be used from now on.
:::

---

## Aftermath

Once the raid has concluded, and the meltdown/freezedown is over (or if it was a chaos, the raid host announces it's over), you should order all PBST to head to the PBDSF. 

At the PBDSF, PBST should head to the PBST STS. Here, you can tell PBST about any information about the raid, such as what PBST has achieved, the positives of the raid, or if someone didn't meet your expectations you can remind PBST of what you expect from them and/or tell them to read the handbook etc. 

You can also give the bonus here too. You can decide who deserves the bonus in many different ways. The two common ways is decide who deserves it yourself, or tell PBST to whisper you on who deserves to get the bonus. You can also just not give the bonus at all. After you're done with everything, you can dismiss PBST.

---

## Raid logs

Once everything is concluded, you can then log the attendance by using the #raid-log channel. On this channel, you'll post the raid attendance of every PBST that have attended the raid. 

*NOTE:* SINCE the 27th of november 2020, the points you give is based on what level the raid was. The following is the maximum base points you can give as an RRL:

**Base points for Level 0: 2**

**Base points for Level 1: 4**

**Base points for Level 2: 6**

**Base points for Level 3: 8**

For the bonus, it is usually best to just add one or two extra points. Some RRLs may even give the bonus to two different people who have performed exceptionally well. You can choose whatever you feel is fine to you, but ensure that it isn't over-the-top. 

If someone broke handbook rules, didn't listen to your orders or was toxic to people during the raid etc, you can give a point deduction. Typically, you should start with -5 points, as well as outlining why they deserve the -5+ points. If the offense was serious, it is best to add evidence if you were able to catch them. **ONLY** add deductions if the person deserves it. 

---

There may be more about the duties of a RRL, but this guide presents the main bits of it. If you follow this guide, your chances of making a mistake will dramatically reduce or you will not make any mistakes at all. If there are still any questions you may have, please reach out to your other RRLs, they'll be sure to help. Good luck and have fun out there!  
