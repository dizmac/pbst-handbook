# Important facilities to patrol
## Pinewood Computer Core (PBCC)
Pinewood Computer Core is Pinewood Builders’ most frequently visited game, making it the main place of interest for PBST to patrol.

PBCC has an anti-spawnkill protection in place, which makes it impossible to kill anyone at the spawn area. This protection extends through the shop and stairwell and ends past the yellow line at the bottom of the elevator.
– Should you discover your weapon does allow you to kill at spawn, don’t spawnkill. This overrides any rules established elsewhere in the handbook.
– Camping right outside the spawn border and blocking it is also considered spawnkilling.

Your main objective in this game is to prevent the core from melting or freezing. The following chart shows you how different settings affect the temperature:  
![img](/core_temps.png)

### Coolant
The coolant is the most vital piece of the core controls, doing a -14° cooldown effect when in operation. It’s also the most vulnerable, and several items need to be set to keep the coolant going.

 * The coolant pumps must be turned on.
 * The coolant production needs to be active, since the pumps will shut down without coolant in the tank. Trains can also supply additional coolant.
 * In Admin control, the coolant supply button must be turned on.
 * Additionally, the coolant pipe at the mainframe can be sabotaged, reducing its cooling effect to -12°. If the coolant works beneficial to PBST, you can shoot people who sabotage the coolant without warning. However, if the core is approaching freezedown, PBST can sabotage the coolant as well. This may only be done if the coolant is working disadvantageous to PBST.

### Emergency Coolent
If your efforts fail and a meltdown does occur, you can go to the Emergency Coolant at Sector G to try to rescue the core. Use the Security Code to get in (the code that’s announced in-game) and bring the rods to levels between 69% and 81%. Keep as many rods as possible within these levels until the timer hits 0.

Success rates for the number of coolant rods green when the countdown hits 0:
1: 10%
2: 25%
3: 90%

Successful activation of the E-coolant will set the temperature back to 3500 degrees.

Aside from melt- and freezedown, PBCC also features various ‘disasters’, some of which require PBST to evacuate the visitors. In the event of a plasma surge, earthquake, or radiation flood, bring the people in danger to safety.

Mutants also often appear at PBCC, if somebody steps into the pool of leaking radioactive fluid they become a mutant. Mutants are to be killed at once. PBST members may not become mutants while on duty.

### Trains
PBST can clean up derailed trains if they’re blocking the tracks, preventing other trains from going by. Find the driver’s seat and sit on it, then click “remove train”. You are free to use your taser if somebody is still in the driver’s seat and refuses to remove the train himself.

Nuke trains are an additional danger since the nukes can explode, killing everyone nearby. Remove these trains as quickly as possible if any nukes are flashing red. If you see someone trying to explode a nuke train on purpose, you can kill that person without warning.

Trains and other vehicles can be flown to places they shouldn’t be at. If you see someone flying a vehicle, you can use your taser to stop them. Remove any vehicle that has been flown.


## PBST Activity Center (PBSTAC)
The PBST Activity Center is the main facility for trainings and various other activities. You can practice one of the obbies like the Gamma Obby, the Vortex of Rage, or the Tall Towers. It also features a simulation of the core at PBCC with all the variables to change the temperature, and it will melt- or freeze down too.

PBSTAC can be visited by Raiders, who will kill mercilessly with their OP Weapons. They are KoS and patrolling PBST members have to join forces to keep them at bay.

Also be on the lookout for monsters like zombies, skeletons, or worse…
Try to work together and make sure the core stays in balance.


